"use strict";

// Теоретичні питання

// 1 - Для того що б не дублювати(повторювати багато разів) якусь частину коду, можна та треба використовувати функції. Наприклад якесь повідомлення яке має вспливати в різних місцях сайту, якась операція яка має повторюватись в різних частинах коду.
// 2 - Аргументи це значення які потім ця функція і використовує при опрацюванні.
// 3 - Оператор return повертає значення яке було отримано в результаті. Нічого не виводить в консоль чи в alert, тощо.

// Завдання практичне

let firstNumber = prompt("Enter first number");

let secondNumber = prompt("Enter second number");

let operation = prompt("Enter your operation");

let add = function(a,b){
    return a + b;
}

let subtract = function(a,b){
    return a - b;
}

let multiply = function(a,b){
    return a * b;
}

let divide = function(a,b){
    return a / b;
}
switch (operation) {

    case "+" :
        operation = add;
        break;
    case "-" :
        operation = subtract;
        break;

    case "*" :
        operation = multiply;
        break;

    case "/" :
        operation = divide;
        break;

    default : operation = prompt("Enter your operation");

}
const calculate = function(a,b,operation){
    return operation(a, b)
}


console.log(calculate(+firstNumber,+secondNumber,operation));
